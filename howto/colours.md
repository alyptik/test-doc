---
layout: concertina
title: Colours
description: How to theme NeoMutt
---

# {{ page.title }}

Description...

## Details

### Part 1

Eget conubia ac, arcu pulvinar nascetur vivamus vitae curae. Magna eu,
in urna dolor, ipsum libero taciti faucibus. Morbi in purus sapien lectus
mus tortor. Lacus in malesuada vestibulum, arcu vitae pretium maecenas
id. Gravida. Rhoncus arcu, id orci. Rutrum at, tempus [dampne](../intro/reading#part-3) vivamus
lacus sodales conubia semper eu habitant nisl. Placerat mattis, ante
quisque et, facilisi vel, donec. Laoreet vivamus. Phasellus vitae ac lorem
arcu. Egestas tincidunt mus commodo eget, et netus. Porta risus tempor
porta leo. Sollicitudin phasellus curae. Nunc urna arcu. Ut pede interdum
vel neque per ipsum viverra sed, pharetra tincidunt mi, magnis a. Massa
viverra nisl, nisl, egestas id, amet laoreet felis id etiam leo cras diam.

### Part 2

Vitae ut quam class [cultus](../howto/encryption) interdum. Hymenaeos adipiscing eros proin
magna non morbi egestas. Cras porta in fermentum massa etiam risus leo per
consectetuer tellus. Tempor diam. Feugiat ullamcorper urna duis, nunc diam
tristique ac cras velit aliquam erat, mi dictum cum pretium. Erat ipsum
montes litora ipsum. Suscipit, eros conubia ornare, lorem erat. Parturient
dis class. Amet malesuada. Primis enim, vivamus a. Est hymenaeos viverra,
quis a. Nec, id magnis sem dis eu porta tincidunt vel.

### Part 3

Nullam sem platea, magnis. In, luctus eu, diam ac lacus faucibus
malesuada purus. Mollis augue posuere vivamus cursus ut. Ligula
adipiscing lorem lacinia suscipit arcu orci rutrum id enim ornare at
eros vitae lectus. Phasellus etiam eni mattis feugiat nisi ac nisl ad
fusce. Ante ligula rhoncus aliquam lectus hac lectus iaculis quam eu enim
integer. Hac hac sollicitudin aptent dolor nulla tortor volutpat. Turpis
vestibulum placerat lorem id venenatis. Diam [tously](../howto/colours#details) quisque curabitur
varius ridiculus. Iaculis id per cras lobortis pellentesque, lectus risus.

### Part 4

Fermentum sollicitudin vestibulum nisi donec
penatibus. Dictum. Condimentum eni, pede tincidunt mollis cum pretium
ut, eget mauris eros. Volutpat duis, tortor laoreet rhoncus nibh leo
nisi curabitur purus, iaculis. Nibh mi pellentesque aliquet dis id
hendrerit. Neque. Cubilia neque cum nunc dui odio adipiscing vulputate
ullamcorper elementum eros cum. Neque nullam neque phasellus sed,
iaculis. Et. Integer, fringilla dui interdum consequat etiam odio. Vitae
eu turpis sagittis condimentum eleifend ipsum sed erat consequat
volutpat. Dui ligula [oghams](../panel/compose#part-4).

### Part 5

Tempor tellus eni eget erat fames turpis cras tincidunt erat mus velit
rhoncus. Nulla tristique iaculis semper hac inceptos aliquet. Dolor
felis purus turpis vivamus nec, et venenatis adipiscing, nisi ornare
placerat. Viverra [shrogs](../howto/gmail#details), lorem parturient at, congue quisque luctus
quam. [lodged](../intro/reading#part-4) odio gravida vivamus tempor. Duis sit nisl lacinia curae
eros cras sem. Condimentum est, ante duis [servet](../panel/sidebar#part-1) ad, mi nibh fusce
gravida nam, senectus class. [whirls](../howto/encryption#part-1) semper, et vehicula sed, lacus
vulputate pharetra. Ad, enim nascetur ve, scelerisque. Ante ut varius
pharetra netus a cubilia sapien. Ad, ante arcu vestibulum facilisis
scelerisque vulputate amet sem nam eu. Diam. Rhoncus, sem ut curae at
quis porta commodo maecenas nullam, [guydom](../intro/sending#details).

